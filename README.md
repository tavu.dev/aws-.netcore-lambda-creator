**# AWS .NETCORE LAMBDA CREATOR**

This project aims to help developer on MACOS quickly create and deploy an AWS Lambda function. 

**# Requirements** 

MACOS and Docker 

**# 1. Clone this project**

    git clone https://gitlab.com/tavu.dev/aws-.netcore-lambda-creator.git

**# 2. Move into the project root folder**

    cd aws-.netcore-lambda-creator

**# 3. Build docker image**: make sure that you change YOUR_AWS_ACCESS_ID and YOUR_AWS_SECRET_KEY to your AWS credentials

    docker build -t tav_aws_lambda_netcore:1.0 . --build-arg access_id=YOUR_AWS_ACCESS_ID --build-arg secret_key=YOUR_AWS_SECRET_KEY

**#4. Add run permission to start.sh, program.sh and deploy_lambda.sh**

    sudo chmod +x start.sh
    sudo chmod +x program.sh

**# 5. Start the image with start.sh**

    ./start.sh

**# 7. Debug with VSCODE**: this can be use with install the Remote - Containers (ms-vscode-remote.remote-containers
) plugin for VSCODE to debug the function.

Mount /src on MacOS to Repo folder
sudo nano /etc/synthetic.conf
content
src /Users/tavu.dev/repos
Add sharing method on docker

**# 8. Force OmniSharp to reload project if failed: cmd + shift + P > OmniSharp 


