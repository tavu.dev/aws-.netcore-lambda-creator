#! /bin/bash

ID=$(docker ps -aqf "name=tav_lambda_dev")
if ! test -z "$ID"
then
    docker kill $ID
fi
docker run -v "${PWD}"/../:/src -v /var/run/docker.sock:/var/run/docker.sock -w /src --name tav_lambda_dev --rm -it tav_aws_lambda_netcore:1.1

/Users/tavu.dev