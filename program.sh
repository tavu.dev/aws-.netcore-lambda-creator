#! /bin/bash
export PATH="$PATH:/root/.dotnet/tools"

ACTION=0
PROGRAM_ROOT=$PWD
PROJECT_NAME=''


function main(){
    if test -z "$PROJECT_NAME"
    then
        echo "1. Create new empty AWS Lambda project"
        echo "2. Load an existed AWS Lambda project"
        echo "3. Exit"
        read ACTION
        if test $ACTION -eq 3
        then
            exit
            #docker kill $(docker ps -aqf "name=tav_lambda_dev")
        fi
        echo "Please enter project name"
        read PROJECT_NAME

        if test $ACTION -eq 1
        then
            if test -d $PROJECT_NAME
            then
                echo "There is a folder with $PROJECT_NAME name. Please choose different name."
            else
                echo "Creating new empty AWS Lambda project with name $PROJECT_NAME ..."
                dotnet new lambda.EmptyFunction --name $PROJECT_NAME
            fi
        elif test $ACTION -eq 2
        then
            if ! test -d $PROJECT_NAME 
            then
                echo "There is no folder with $PROJECT_NAME name. Please try again"
                PROJECT_NAME=''
            else
                echo "Restoring projects ..."
                dotnet restore $PROJECT_NAME/src/$PROJECT_NAME
                dotnet restore $PROJECT_NAME/test/$PROJECT_NAME.Tests
                echo "$PROJECT_NAME is loaded"
            fi
        else
            echo "Invalid selection."
        fi
    else
        echo "1. Build $PROJECT_NAME project"
        echo "2. Build $PROJECT_NAME.Tests project"
        echo "3. Run $PROJECT_NAME.Tests project"
        echo "4. Deploy $PROJECT_NAME"
        echo "5. Invoke $PROJECT_NAME"
        echo "6. Exit"
        read ACTION
        if test $ACTION -eq 6
        then
            exit
            #docker kill $(docker ps -aqf "name=tav_lambda_dev")
        fi

        if test $ACTION -eq 1
        then 
            echo "Building $PROJECT_NAME ..."
            dotnet build $PROJECT_NAME/src/$PROJECT_NAME
        elif test $ACTION -eq 2
        then
            echo "Building $PROJECT_NAME.Tests ..."
            dotnet build $PROJECT_NAME/test/$PROJECT_NAME.Tests
        elif test $ACTION -eq 3
        then
            echo "Testing $PROJECT_NAME ..."
            dotnet test $PROJECT_NAME/test/$PROJECT_NAME.Tests
        elif test $ACTION -eq 4
        then
            echo "Deploying $PROJECT_NAME ..."
            cd $PROJECT_NAME/src/$PROJECT_NAME/
            dotnet lambda deploy-function $PROJECT_NAME
            cd $PROGRAM_ROOT
        elif test $ACTION -eq 5
        then
            read PAYLOAD
            dotnet lambda invoke-function $PROJECT_NAME --payload $PAYLOAD
        else
            echo "Invalid selection."
        fi
    fi
    main
}
main