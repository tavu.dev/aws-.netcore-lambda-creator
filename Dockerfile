FROM ubuntu:20.04

ARG access_id
ARG secret_key
RUN mkdir /src
RUN apt-get update
RUN apt-get install -y wget

RUN wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb

RUN  apt-get update
RUN apt-get install -y apt-transport-https && apt-get update && apt-get install -y dotnet-sdk-3.1

RUN apt-get install zip -y
RUN apt-get install unzip -y

RUN wget https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip
RUN unzip awscli-exe-linux-x86_64.zip
RUN ls -la
RUN ./aws/install
RUN rm -rf awscli-exe-linux-x86_64.zip
RUN rm -rf aws

RUN wget https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip
RUN unzip aws-sam-cli-linux-x86_64.zip -d sam-installation
RUN ./sam-installation/install

RUN aws configure set aws_access_key_id ${access_id}
RUN aws configure set aws_secret_access_key ${secret_key}
RUN aws configure set region us-east-1

RUN dotnet tool install -g Amazon.Lambda.Tools
RUN dotnet new -i Amazon.Lambda.Templates
RUN apt-get install -y git

RUN apt-get install -y curl
RUN apt-get install apt-transport-https ca-certificates curl software-properties-common -y

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
RUN apt-get install -y docker-ce

RUN apt-get install nodejs npm -y
RUN nodejs --version

WORKDIR /src
#ENTRYPOINT ./program.sh
